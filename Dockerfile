FROM python:3.10-slim

WORKDIR /app

COPY exporter.py ./

RUN pip install prometheus-client requests

ENTRYPOINT ["python", "exporter.py"]

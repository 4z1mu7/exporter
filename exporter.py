import time
import os
import requests
import re
from prometheus_client import start_http_server, Gauge
from datetime import datetime

# Initialisation des métriques
festival_drink_count_gauges = {}
festival_shop_gauges = {}
festival_shop_stock_gauges = {}
festival_shop_drink_sales_gauges = {}

POCKETBASE_URL = os.getenv('POCKETBASE_URL')
LOGIN_ENDPOINT = f'{POCKETBASE_URL}/api/admins/auth-with-password'
FESTIVAL_ENDPOINT = f'{POCKETBASE_URL}/api/collections/festival/records?perPage=500000'
CLIENT_ORDER_ENDPOINT = f'{POCKETBASE_URL}/api/collections/clientOrder/records?perPage=500000'
DRINK_ENDPOINT = f'{POCKETBASE_URL}/api/collections/drink/records?perPage=500000'
SHOP_ENDPOINT = f'{POCKETBASE_URL}/api/collections/shop/records?perPage=500000'
QUANTIFIED_ITEM_ENDPOINT = f'{POCKETBASE_URL}/api/collections/quantifiedItem/records'

USERNAME = os.getenv('POCKETBASE_USERNAME')
PASSWORD = os.getenv('POCKETBASE_PASSWORD')

def login_to_pocketbase():
    try:
        response = requests.post(LOGIN_ENDPOINT, json={
            'identity': USERNAME,
            'password': PASSWORD
        })
        response.raise_for_status()
        token = response.json()['token']
        return token
    except requests.RequestException as e:
        print(f"Error logging in to PocketBase: {e}")
        return None

def fetch_festivals(token):
    headers = {'Authorization': f'Bearer {token}'}
    try:
        response = requests.get(FESTIVAL_ENDPOINT, headers=headers)
        response.raise_for_status()
        return response.json()['items']
    except requests.RequestException as e:
        print(f"Error fetching festivals: {e}")
        return []

def fetch_client_orders(token):
    headers = {'Authorization': f'Bearer {token}'}
    try:
        response = requests.get(CLIENT_ORDER_ENDPOINT, headers=headers)
        response.raise_for_status()
        return response.json()['items']
    except requests.RequestException as e:
        print(f"Error fetching client orders: {e}")
        return []

def fetch_drinks(token):
    headers = {'Authorization': f'Bearer {token}'}
    try:
        response = requests.get(DRINK_ENDPOINT, headers=headers)
        response.raise_for_status()
        return response.json()['items']
    except requests.RequestException as e:
        print(f"Error fetching drinks: {e}")
        return []

def fetch_shops(token):
    headers = {'Authorization': f'Bearer {token}'}
    try:
        response = requests.get(SHOP_ENDPOINT, headers=headers)
        response.raise_for_status()
        return response.json()['items']
    except requests.RequestException as e:
        print(f"Error fetching shops: {e}")
        return []

def fetch_quantified_item(token, item_id):
    headers = {'Authorization': f'Bearer {token}'}
    try:
        response = requests.get(f'{QUANTIFIED_ITEM_ENDPOINT}/{item_id}', headers=headers)
        response.raise_for_status()
        return response.json()
    except requests.RequestException as e:
        print(f"Error fetching quantified item: {e}")
        return None

def is_within_festival_dates(record_date_str, festival_start_str, festival_end_str):
    try:
        record_date = datetime.strptime(record_date_str[:-5], "%Y-%m-%d %H:%M:%S")
        festival_start = datetime.strptime(festival_start_str[:-5], "%Y-%m-%d %H:%M:%S")
        festival_end = datetime.strptime(festival_end_str[:-5], "%Y-%m-%d %H:%M:%S")

        return festival_start <= record_date <= festival_end
    except ValueError as e:
        print(f"Error parsing date: {record_date_str}. Exception: {e}")
        return False

def update_festival_metrics(token):
    festivals = fetch_festivals(token)
    client_orders = fetch_client_orders(token)
    drinks = fetch_drinks(token)
    shops = fetch_shops(token)

    for festival in festivals:
        festival_name = festival['name']
        festival_start = festival['start']
        festival_end = festival['end']

        drink_counts = {drink['id']: 0 for drink in drinks}
        shop = {shop['id']: 0 for shop in shops}
        shop_drink_sales = {(shop['id'], drink['id']): 0 for shop in shops for drink in drinks}
        shop_drink_stock = {(shop['id'], drink['id']): 0 for shop in shops for drink in drinks}

        for order in client_orders:
            if is_within_festival_dates(order['created'], festival_start, festival_end):
                shop_id = order['bar']
                for item_id in order['items']:
                    item = fetch_quantified_item(token, item_id)
                    if item['item']:
                        drink_id = item['item']
                        quantity = item['quantity']
                        drink_counts[drink_id] += quantity
                        shop[shop_id] += quantity
                        shop_drink_sales[(shop_id, drink_id)] += quantity         
            
        for shop_stock in shops:
            for item_id in shop_stock['items']:
              item = fetch_quantified_item(token, item_id)
              if item['item']:
                drink_id = item['item']
                quantity = item['quantity']
                shop_drink_stock[(shop_id, drink_id)] = quantity
        
        for drink_id, count in drink_counts.items():
            drink_name = next((drink['name'] for drink in drinks if drink['id'] == drink_id), None)
            drink_volume = next((drink['volume'] for drink in drinks if drink['id'] == drink_id), None)
            if drink_name:
                gauge_name = re.sub(r'[^a-zA-Z0-9]+', '_', f'pocketbase_festival_{festival_name}_drink_{drink_name}_volume_{drink_volume}_count')
                if gauge_name not in festival_drink_count_gauges:
                    festival_drink_count_gauges[gauge_name] = Gauge(gauge_name, f'Number of {drink_name} drinks in {festival_name} festival')
                festival_drink_count_gauges[gauge_name].set(count)
        
        for shop_id, sales in shop.items():
            shop_name = next((shop['label'] for shop in shops if shop['id'] == shop_id), None)
            if shop_name:
                gauge_name = re.sub(r'[^a-zA-Z0-9]+', '_', f'pocketbase_festival_{festival_name}_shop_{shop_name}_sales_count')
                if gauge_name not in festival_shop_gauges:
                    festival_shop_gauges[gauge_name] = Gauge(gauge_name, f'Total sales in {shop_name} during {festival_name} festival')
                festival_shop_gauges[gauge_name].set(sales)
        
        for (shop_id, drink_id), sales in shop_drink_sales.items():
            shop_name = next((shop['label'] for shop in shops if shop['id'] == shop_id), None)
            drink_name = next((drink['name'] for drink in drinks if drink['id'] == drink_id), None)
            drink_volume = next((drink['volume'] for drink in drinks if drink['id'] == drink_id), None)
            if shop_name and drink_name:
                gauge_name = re.sub(r'[^a-zA-Z0-9]+', '_', f'pocketbase_festival_{festival_name}_shop_{shop_name}_drink_{drink_name}_volume_{drink_volume}_sales_count')
                if gauge_name not in festival_shop_drink_sales_gauges:
                    festival_shop_drink_sales_gauges[gauge_name] = Gauge(gauge_name, f'Sales of {drink_name} ({drink_volume}) in {shop_name} during {festival_name} festival')
                festival_shop_drink_sales_gauges[gauge_name].set(sales)

        for (shop_id, drink_id), stock in shop_drink_stock.items():
            shop_name = next((shop['label'] for shop in shops if shop['id'] == shop_id), None)
            drink_name = next((drink['name'] for drink in drinks if drink['id'] == drink_id), None)
            drink_volume = next((drink['volume'] for drink in drinks if drink['id'] == drink_id), None)
            if shop_name and drink_name:
                gauge_name = re.sub(r'[^a-zA-Z0-9]+', '_', f'pocketbase_shop_{shop_name}_drink_{drink_name}_volume_{drink_volume}_stock')
                if gauge_name not in festival_shop_stock_gauges:
                    festival_shop_stock_gauges[gauge_name] = Gauge(gauge_name, f'Stock of {drink_name} ({drink_volume}) in {shop_name}')
                festival_shop_stock_gauges[gauge_name].set(stock)

def collect_metrics():
    token = login_to_pocketbase()
    if not token:
        print("Failed to log in to PocketBase, exiting.")
        return
    
    while True:
        update_festival_metrics(token)
        time.sleep(10)

if __name__ == '__main__':
    start_http_server(8000)
    collect_metrics()
